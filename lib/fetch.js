const axios = require('axios')
const axiosRetry = require('axios-retry')
const randomUseragent = require('random-useragent')
const debug = require('debug')('pageit')

axios.defaults.headers.common['user-agent'] = randomUseragent.getRandom(ua => ua.browserName === 'Firefox' || ua.browserName === 'Chrome')
axiosRetry(axios, { retries: 5 })

function fetch (url, opts) {
  if (opts.proxies.length > 0) {
    // TODO: allow host ip [without proxy option]
    let proxies = opts.proxies[Math.floor(Math.random() * opts.proxies.length)].split(':')
    debug('proxies:', proxies)
    opts.proxy = {
      host: proxies[0],
      port: proxies[1]
    }
  }
  if (opts.method && opts.method.toLowerCase() === 'post') {
    // 'Content-Type': 'application/x-www-form-urlencoded'
    opts.data = url.split('?')[1] || ''
  }
  if (opts.randomUseragent) {
    let headers = opts.headers || {}
    headers['User-Agent'] = randomUseragent.getRandom(ua => ua.browserName === 'Firefox' || ua.browserName === 'Chrome')
    opts.headers = headers
  }
  return axios(url, opts).then(res => res.data)
}

module.exports = fetch
