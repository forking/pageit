const isObject = require('isobject')
const { isUrl } = require('../../utils')

// Get the root, if there is one.
function root (selector) {
  return (typeof selector === 'string' || Array.isArray(selector)) &&
  !~selector.indexOf('@') &&
  !isUrl(selector) &&
  selector
}

// Compact an array, removing empty objects
function compact (arr) {
  return arr.filter(val => {
    if (!val) return false
    if (val.length !== undefined) return val.length !== 0
    for (let key in val) {
      if (Object.prototype.hasOwnProperty.call(val, key)) {
        return true
      }
    }
    return false
  })
}

module.exports = {
  root: root,
  compact: compact,
  isObject: isObject
}
