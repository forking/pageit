const debug = require('debug')('resolve')
const parse = require('./parse')

function resolve ($, scope, selector, filters) {
  debug('resolve($j, %j)', scope, selector)
  filters = filters || {}
  let array = Array.isArray(selector)
  let obj = parse(array ? selector[0] : selector)
  obj.attribute = obj.attribute || 'text'

  if (!obj.selector) {
    obj.selector = scope
    scope = null
  }

  let value = find($, scope, array ? [obj.selector] : obj.selector, obj.attribute)
  debug('resolved($j, %j) => %j', scope, selector, value)

  if (array && typeof value.map === 'function') {
    value = value.map(v => filter(obj, $, scope, selector, v, filters)
    )
  } else {
    value = filter(obj, $, scope, selector, value, filters)
  }

  return value
}

// Find the node(s)
function find ($, scope, selector, attr) {
  if (scope && Array.isArray(selector)) {
    let $scope = select($, scope)
    let out = []
    $scope.map(i => {
      let $el = $scope.eq(i)
      let $children = select($el, selector[0])
      $children.map(i => {
        out.push(attribute($children.eq(i), attr))
      })
    })
    return out
  } else if (scope) {
    let $scope = select($, scope)
    return attribute($scope.find(selector).eq(0), attr)
  } else {
    let $selector
    if (Array.isArray(selector)) {
      $selector = select($, selector[0])
      let out = []
      $selector.map(i => {
        out.push(attribute($selector.eq(i), attr))
      })
      return out
    } else {
      $selector = select($, selector)
      return attribute($selector.eq(0), attr)
    }
  }
}

// Selector abstraction, deals
function select ($, selector) {
  if ($.is && $.is(selector)) return $
  return $.find ? $.find(selector) : $(selector)
}

// Select the attribute based on `attr`
function attribute ($el, attr) {
  switch (attr) {
    case 'html':
      return $el.html()
    case 'text':
      return $el.text()
    default:
      return $el.attr(attr)
  }
}

// Filter the value(s)
function filter (obj, $, scope, selector, value, filters) {
  let ctx = { $: $, selector: obj.selector, attribute: obj.attribute }
  return (obj.filters || []).reduce((out, filter) => {
    let fn = filters[filter.name]
    if (typeof fn === 'function') {
      let args = [out].concat(filter.args || [])
      let filtered = fn.apply(ctx, args)
      debug('%s.apply(ctx, %j) => %j', filter.name, args, filtered)
      return filtered
    } else {
      throw new Error('Invalid filter: ' + filter.name)
    }
  }, value)
}

module.exports = resolve
