/**
 * html-parser
 * Fork [d9996bc](https://github.com/lapwinglabs/x-ray)
 */

const _ = require('lodash')
const cheerio = require('cheerio')
const debug = require('debug')('parse')
const WalkHTML = require('./walk-html')
const { isObject } = require('./util')
const params = require('./params')

function HtmpParser (options) {
  options = options || {}
  let filters = options.filters || {}

  function htmpParser (source, scope, selector) {
    let args = params(source, scope, selector)
    selector = args.selector
    source = args.source
    scope = args.context

    let walkHTML = WalkHTML(htmpParser, selector, scope, filters)

    function node (source2, fn) {
      if (arguments.length === 1) {
        fn = source2
      } else {
        source = source2
      }

      debug('params: %j', {
        source: source,
        scope: scope,
        selector: selector
      })

      if (source) {
        let $ = source.html ? source : cheerio.load(source)
        walkHTML($, next)
      } else {
        debug('%s is not a url or html. Skipping!', source)
        return walkHTML(cheerio.load(''), next)
      }

      function next (err, obj, $) {
        if (err) return fn(err)
        fn(null, obj)
      }
      return node
    }
    return node
  }
  return htmpParser
}

module.exports = (...args) => {
  let filters = {}
  if (args.length < 2) {
    throw new Error('Miss selector argument.')
  // } else if (args.length === 2) {
    // source, selector
  } else if (args.length === 3) {
    if (isObject(args[2])) {
      let isFilters = false
      _.mapValues(args[2], v => {
        isFilters = _.isFunction(v)
      })
      if (isFilters) {
        // source, selector, filters
        filters = args[2]
        args = args.slice(0, 2)
      }
    }
    // else: source, scope, selector
  } else if (args.length > 3) {
    // source, scope, selector, filters, ...
    filters = args[3]
    args = args.slice(0, 3)
  }
  return new HtmpParser({ filters: filters })(...args)
}
