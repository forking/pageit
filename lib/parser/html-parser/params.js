const { isHTML } = require('../../utils')

function params (source, context, selector) {
  let args = {}
  if (undefined === context) {
    args.source = null
    args.context = null
    args.selector = source
  } else if (undefined === selector) {
    if (isHTML(source)) {
      args.source = source
      args.context = null
    } else {
      args.source = null
      args.context = source
    }
    args.selector = context
  } else {
    args.source = source
    args.context = context
    args.selector = selector
  }

  return args
}

module.exports = params
