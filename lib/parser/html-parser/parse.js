const rselector = /^([^@]*)(?:@\s*([\w-_:]+))?$/
const rfilters = /\s*\|(?!=)\s*/

function filterParser (str) {
  return str.split(/ *\| */).map(call => {
    let parts = call.split(':')
    let name = parts.shift()
    let args = parseArgs(parts.join(':'))

    return {
      name: name,
      args: args
    }
  })
}

function parseArgs (str) {
  let args = []
  let re = /"([^"]*)"|'([^']*)'|([^ \t,]+)/g
  let m

  while (m = re.exec(str)) { // eslint-disable-line
    args.push(m[2] || m[1] || m[0])
  }

  return args
}

function parse (str) {
  let filters = str.split(rfilters)
  let z = filters.shift()
  let m = z.match(rselector) || []

  return {
    selector: m[1] ? m[1].trim() : m[1],
    attribute: m[2],
    filters: filters.length ? filterParser(filters.join('|')) : []
  }
}

module.exports = parse
