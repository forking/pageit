const Batch = require('batch')
const { compact, root, isObject } = require('./util')
const resolve = require('./resolve')

// Walk recursively, providing callbacks for each step.
function walk (value, fn, done, key) {
  let batch = Batch()
  let out
  if (isObject(value)) {
    out = {}
    Object.keys(value).forEach(k => {
      let v = value[k]
      batch.push(next => {
        walk(v, fn, (err, value) => {
          if (err) return next(err)
          // ignore undefined values
          if (undefined !== value && value !== '') {
            out[k] = value
          }
          next()
        }, k)
      })
    })
  } else {
    out = null
    batch.push(next => {
      fn(value, key, (err, v) => {
        if (err) return next(err)
        out = v
        next()
      })
    })
  }

  batch.end(err => {
    if (err) return done(err)
    return done(null, out)
  })
}

function WalkHTML (xray, selector, scope, filters) {
  return ($, fn) => {
    walk(selector, (v, k, next) => {
      if (typeof v === 'string') {
        let value = resolve($, root(scope), v, filters)
        return next(null, value)
      } else if (typeof v === 'function') {
        return v($, (err, obj) => {
          if (err) return next(err)
          return next(null, obj)
        })
      } else if (Array.isArray(v)) {
        if (typeof v[0] === 'string') {
          return next(null, resolve($, root(scope), v, filters))
        } else if (typeof v[0] === 'object') {
          let $scope = $.find ? $.find(scope) : $(scope)
          let pending = $scope.length
          let out = []

          // Handle the empty result set (thanks @jenbennings!)
          if (!pending) return next(null, out)

          $scope.each((i, el) => {
            let $innerscope = $scope.eq(i)
            let node = xray(scope, v[0])
            node($innerscope, (err, obj) => {
              if (err) return next(err)
              out[i] = obj
              if (!--pending) {
                return next(null, compact(out))
              }
            })
          })
          // Nested crawling broken on 'master'. When to merge 'bugfix/nested-crawling' #111, Needed to exit this without calling next, the problem was that it returned to the "finished" callback before it had retrived all pending request. it should wait for "return next(null, compact(out))"
          return
        }
      }
      return next()
    }, (err, obj) => {
      if (err) return fn(err)
      fn(null, obj, $)
    })
  }
}

module.exports = WalkHTML
