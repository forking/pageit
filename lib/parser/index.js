const { isHTML } = require('../utils')
const HtmlParser = require('./html-parser')
const JsonParser = require('./json-parser')
const debug = require('debug')('pageit:parse')

function Parser (...args) {
  // args: source, scope, selector, filters
  let results = args[0]
  if (isHTML(args[0])) {
    debug('isHTML')
    // source,[ scope,] selector
    return new Promise((resolve) => {
      HtmlParser(...args)((err, res) => {
        if (err) {
          debug('ERR:', err)
        }
        resolve(res)
      })
    })
  } else if (args[2]) {
    debug('isJSON')
    // source, selector
    if (!args[1]) {
      args.splice(1, 1)
    }
    return JsonParser(...args, null, false)
  }
  return results
}

module.exports = Parser
module.exports.HtmlParser = HtmlParser
module.exports.JsonParser = JsonParser
