module.exports = {
  trim: value => typeof value === 'string' ? value.trim() : value,
  reverse: value => typeof value === 'string' ? value.split('').reverse().join('') : value,
  slice: (value, start, end) => typeof value === 'string' ? value.slice(start, end) : value
}
