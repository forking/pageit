const urlRegex = require('url-regex')({exact: true})

// Check if the string is HTML
function isHTML (str) {
  str = (str || '').toString().trim()
  return str[0] === '<' && str[str.length - 1] === '>'
}

// Check if the string is url
function isUrl (url) {
  if (typeof url !== 'string') {
    return false
  }

  return urlRegex.test(url.trim())
}

module.exports = {
  isUrl,
  isHTML
}
