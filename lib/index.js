const debug = require('debug')('pageit')
const _ = require('lodash')
const delay = require('delay')
const cheerio = require('cheerio')
const { isUrl, isHTML } = require('./utils')
const parser = require('./parser')
const filters = require('./filters')
const fetch = require('./fetch')

let urls = []
let options = {
  name: 'pageit',
  startUrls: [],
  filters: filters,
  // delay the next request for `ms` milliseconds
  delay: 0,
  // scope is optional for html mode
  scope: '',
  selector: '',
  fetchConfig: {
    // ['x.x.x.x:port']
    proxies: [],
    // random useragent
    randomUseragent: false
  },
  // actions
  fetch: fetch,
  extract: result => result,
  next: (source, result, urls) => {},
  save: (result, url) => {},
  done: () => {}
  // TODO: error patch
}

function pageit (config = {}) {
  options = _.defaultsDeep(config, options)
  if (Array.isArray(options.startUrls)) {
    urls = options.startUrls
  } else {
    urls = [options.startUrls]
  }
  let { proxies } = config.fetchConfig
  if (proxies && !Array.isArray(proxies)) {
    options.fetchConfig.proxies = [proxies]
  }
  scrape()
}

function scrape () {
  if (urls.length > 0) {
    let url = urls.pop()
    if (!isUrl(url)) {
      debug('Wrong url:', url)
      scrapeNext()
      return
    }
    start(url)
      .then(res => _parse(res))
      .then(res => {
        if (!res || res.length === 0) return
        return options.save(res, url)
      })
      .then(() => {
        scrapeNext()
      })
      .catch(err => {
        if (err.response && err.response.status === 403) {
          debug('Get the 403 forbidden error, please check this.')
          scrapeDone()
        } else if (err.code === 'ECONNREFUSED' || err.code === 'ECONNRESET') {
          debug(`${err.code} ERROR.`)
          scrapeDone()
        } else {
          debug('Error:', err.message || err)
          scrapeNext()
        }
      })
  } else {
    scrapeDone()
  }
}

function scrapeNext () {
  if (urls.length > 0) {
    return delay(options.delay).then(scrape)
  }
  return scrape()
}

function scrapeDone () {
  return delay(200).then(options.done)
}

function start (url) {
  debug('Fetch:', url)
  return Promise.resolve(options.fetch(url, options.fetchConfig))
}

function _parse (source) {
  return Promise.resolve(parser(source, options.scope, options.selector, options.filters)).then(result => {
    result = options.extract(result)
    return _updateNextUrl(source, result)
  })
}

function _updateNextUrl (source, result) {
  return _parseNextUrl(source, result)
    .then((nextUrl) => {
      if (!nextUrl) {} else if (Array.isArray(nextUrl)) {
        nextUrl.forEach(url => {
          if (isUrl(url)) {
            urls.push(url)
          }
        })
      } else if (!isUrl(nextUrl)) {
        debug('Invalid url:', nextUrl)
      } else {
        urls.push(nextUrl)
      }
      return result
    })
}

function _parseNextUrl (source, result) {
  let nextUrl = ''
  if (typeof options.next === 'string') {
    nextUrl = parser(source, options.next, options.filters)
  } else if (options.next instanceof Function) {
    // support return promise as next
    try {
      nextUrl = isHTML(source, result) ? options.next(cheerio.load(source), result, urls) : options.next(source, result, urls)
    } catch (e) {
      nextUrl = ''
    }
  }
  return Promise.resolve(nextUrl)
}

module.exports = pageit
